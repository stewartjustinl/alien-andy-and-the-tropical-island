﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControllerPeter : MonoBehaviour
{
    public Transform target;
    public Transform pivot;
    //public Vector3 offset;
    public float offset;
    public bool useOffsetValues;

    public float rotateSpeed;
    
    
    float desiredYAngle = 0;
    float desiredXAngle = 90;
    
    public Terrain terrain;
    

    // Start is called before the first frame update
    void Start()
    {
        
        if (!useOffsetValues)
        {
            offset = Vector3.Distance(target.position, transform.position);//target.position - transform.position;
        }

        pivot.transform.position = target.transform.position;
        pivot.transform.parent = target.transform;

        Cursor.lockState = CursorLockMode.Locked;
        
    }

    // Update is called once per frame
    void LateUpdate()
    {
        /*
        float horizontal = Input.GetAxis("Mouse X")*rotateSpeed;
        target.Rotate(0, horizontal, 0);

        float vertical = Input.GetAxis("Mouse Y")*rotateSpeed;
        pivot.Rotate(-vertical, 0, 0);
        
        */
        
        desiredYAngle += Input.GetAxis("Mouse X")*rotateSpeed;//target.eulerAngles.y;
        desiredXAngle += Input.GetAxis("Mouse Y")*rotateSpeed;//pivot.eulerAngles.x;
        target.eulerAngles = new Vector3(target.rotation.x, desiredYAngle+180, target.rotation.z);
        
        if(desiredXAngle > 100 && desiredXAngle < 180)
        {
            desiredXAngle = 100;
        }
        if(desiredXAngle < 15)
        {
            desiredXAngle = 15;
        }
        
        
        
        
        //print(desiredYAngle + " " + desiredXAngle);
        
        float desiredYAngleConv = unityToUnitCircle(desiredYAngle);
        float desiredXAngleConv = unityToUnitCircle(desiredXAngle);
        
        //Quaternion rotation = Quaternion.Euler(desiredXAngle, desiredYAngle, 0);
        Vector3 rotation = new Vector3(Mathf.Cos(desiredYAngleConv)*Mathf.Cos(desiredXAngleConv), Mathf.Sin(desiredXAngleConv), Mathf.Sin(desiredYAngleConv)*Mathf.Cos(desiredXAngleConv));
        
        
        //print(desiredXAngle);
       
        transform.position = target.position + rotation * offset;
        if(transform.position.y < terrainHeightAtHere()+2)
        {
            transform.position = new Vector3(transform.position.x, terrainHeightAtHere()+2/*target.position.y*/, transform.position.z);
        }
     
     transform.LookAt(target);
   
    }
   


//AHH OKAY 
//**THIS** IS HOW TO CONVERT HEIGHT relative to the terrain, into absolute height.
    float terrainHeightAtHere()
    {
        return terrain.SampleHeight(GetComponent<Transform>().position)+terrain.transform.position.y;
    }
    float radToDeg(float rad){return rad/(2*Mathf.PI)*360;}
    float degToRad(float deg){return deg/360 * (2*Mathf.PI);}
    float unityToUnitCircle(float deg){return -(degToRad(deg) - Mathf.PI/2); }
    float unitCircleToUnity(float rad){return radToDeg(-rad + Mathf.PI/2);}
}
