﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopUIScript : MonoBehaviour
{
    // Start is called before the first frame update
    public static GameObject theShopPanel;
    
    
    
    void Start()
    {
        
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if(theShopPanel == null)
        {
            theShopPanel = GameObject.FindGameObjectsWithTag("ShopUI")[0];
            
            
            ShopButtonScript.shopButtons = new List<GameObject>();
            foreach(GameObject g in GameObject.FindGameObjectsWithTag("ShopButton"))
            {
                ShopButtonScript.shopButtons.Add(g);
            }
            
            ShopButtonScript.HideAllShopButtons();
            
            CloseShopPanel();
            
            
            
        }
    }
    public static void OpenShopPanel()
    {
        theShopPanel.SetActive(true);
    }
    public static void CloseShopPanel()
    {
        theShopPanel.SetActive(false);
    }
}

