﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BloodScript : MonoBehaviour
{
    // Start is called before the first frame update
    int timer;
    GameObject bloodEmitter;
    float offset;
    void Start()
    {
        timer = 0;
        tf().eulerAngles = new Vector3(-90,0,0);
    }
    public void Init(GameObject p, float offset)
    {
        bloodEmitter = p;
        this.offset = offset;
        
    }
    

    // Update is called once per frame
    void FixedUpdate()
    {
        if(bloodEmitter == null)
        {
            Destroy(gameObject);
            return;
        }
        timer++;
        tf().position = tf(bloodEmitter).position + Vector3.up * offset;
        if(timer > 100)
        {
            Destroy(gameObject);
        }
    }
    
    
    
    
    
    
    
    
    
    
    
    
    float HDistance(Vector3 a, Vector3 b){ return Sqrt(Sq(a.x-b.x) + Sq(a.z-b.z)); }
    float Sq(float f) { return f*f; }
    float Sqrt(float f) { return Mathf.Sqrt(f); }
    float HAngle(Vector3 a, Vector3 b) { return _GetAngle(a.z,a.x,b.z,b.x); } float _GetAngle(float z1, float x1, float z2, float x2) { if(x1==x2) { if(z1>=z2) { return Mathf.PI/2; } else { return 3*Mathf.PI/2; } } if(x1 > x2) return Mathf.Atan((z2-z1)/(x2-x1)) + Mathf.PI; else return Mathf.Atan((z2-z1)/(x2-x1)); }
    float UnitCircleToUnityAngle(float unitCircle) { return -unitCircle + Mathf.PI/2; }
    float RadToDeg(float rad) { return rad/(Mathf.PI*2)*360; }
    Vector3 HMove(float angle, float distance) { return new Vector3(distance*Mathf.Cos(angle),0,distance*Mathf.Sin(angle)); }
    T g<T>() { return GetComponent<T>(); }
    Transform tf() {return GetComponent<Transform>();}
    Transform tf(GameObject other) {return other.GetComponent<Transform>();}
}
