﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ShopButtonScript : MonoBehaviour
{
    
    
    
    
    public static List<GameObject> shopButtons;
    public int price;
    public string itemToBuy;
    // Start is called before the first frame update
    void Start()
    {
        
        GetComponent<Button>().onClick.AddListener(TaskOnClick);
        
        if(price == 0)
        {
            throw new System.ArgumentException("Price is not set for this button yet!");
        }
        if(itemToBuy.Equals(""))
        {
            throw new System.ArgumentException("Item to buy has not been set for this button yet!");
        }
        
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        GameObject alienAndy = GameObject.FindGameObjectsWithTag("AlienAndy")[0];
        //print("his id is " + alienAndy.GetComponent<PlayerMovement>().inventory.myID);
    }
    void AddItemToInventory(string itemName)
    {
        GameObject alienAndy = GameObject.FindGameObjectsWithTag("AlienAndy")[0];
        alienAndy.GetComponent<Inventory>().AddItem(itemName);
    }
    bool DeductDiamonds(int numDiamonds)
    {
        if(numDiamonds == 0)
        {
            return true;
        }
        GameObject alienAndy = GameObject.FindGameObjectsWithTag("AlienAndy")[0];
        //count diamonds
        int diamonds = 0;
        foreach(Item item in alienAndy.GetComponent<Inventory>().itemList)
        {
            if(item.title.Equals("Diamond Ore"))
            {
                
                diamonds += item.quantity;
            }
        }
        
        //print("has " + diamonds + "diamonds");
        if(numDiamonds > diamonds)
        {
            return false;//cannot deduct this amount
        }
        
        foreach(Item item in new List<Item>(alienAndy.GetComponent<Inventory>().itemList))
        {
            if(item.title.Equals("Diamond Ore"))
            {
                if(item.quantity > numDiamonds)
                {
                    item.quantity -= numDiamonds;
                    return true;
                }
                else
                {
                    numDiamonds -= item.quantity;
                    alienAndy.GetComponent<Inventory>().RemoveItem(item);
                    if(numDiamonds == 0)
                    {
                        return true;
                    }
                    if(numDiamonds < 0)
                    {
                        throw new System.ArgumentException("what");
                    }
                }
            }
        }
        throw new System.ArgumentException("Impossible");
    }
    void TaskOnClick()
    {
        GameObject alienAndy = GameObject.FindGameObjectsWithTag("AlienAndy")[0];
        
        if(DeductDiamonds(price))//check price
            AddItemToInventory(itemToBuy);
        else
        {
            //cannot buy 
        }
        
    }
    public static void ShowShopButton(string item)
    {
        foreach(GameObject g in shopButtons)
        {
            ShopButtonScript sbs = g.GetComponent<ShopButtonScript>();
            if(sbs == null) continue;
            if(sbs.itemToBuy.Equals(item))
            {
                g.SetActive(true);
                return;
            }
        }
        throw new System.ArgumentException("Invalid item name " + item);
        
    }
    public static void HideAllShopButtons()
    {
        foreach(GameObject g in shopButtons)
        {
            g.SetActive(false);
        }
    }
    public static void HideShopButton(string item)
    {
        
        foreach(GameObject g in shopButtons)
        {
            ShopButtonScript sbs = g.GetComponent<ShopButtonScript>();
            if(sbs == null) continue;
            if(sbs.itemToBuy.Equals(item))
            {
                g.SetActive(false);
                return;
            }
        }
        throw new System.ArgumentException("Invalid item name " + item);
        
    }
    
}
