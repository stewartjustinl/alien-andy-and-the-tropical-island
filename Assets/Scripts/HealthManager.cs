﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthManager : MonoBehaviour
{
    public int maxHealth;
    public int currentHealth;
    public Text healthText;

    public PlayerMovement alienAndy;

    public float invincibilityLength;
    private float invincibilityCounter;

    public Renderer playerRenderer1;
    public Renderer playerRenderer2;
    public Renderer playerRenderer3;
    //public Renderer playerRenderer4;
    private float flashCounter;
    public float flashLength = 0.1f;

    private bool isRespawning;
    private Vector3 respawnPoint;
    public float respawnLength;

    public Image blackScreen;
    private bool isFadeToBlack;
    private bool isFadeFromBlack;
    public float fadeSpeed;
    public float waitForFade;
    
    // Start is called before the first frame update
    void Start()
    {
        currentHealth = maxHealth;

        //alienAndy = FindObjectOfType<PlayerMovement>();

        respawnPoint = alienAndy.transform.position;

        healthText.text = "Health: " + currentHealth;
    }

    // Update is called once per frame
    void Update()
    {
        if (invincibilityCounter > 0)
        {
            invincibilityCounter -= Time.deltaTime;

            flashCounter -= Time.deltaTime;

            if(flashCounter <= 0)
            {
              //  playerRenderer1.enabled = !playerRenderer1.enabled;
                //playerRenderer2.enabled = !playerRenderer2.enabled;
                //playerRenderer3.enabled = !playerRenderer3.enabled;
                //playerRenderer4.enabled = !playerRenderer4.enabled;
                flashCounter = flashLength;
            }

            if(invincibilityCounter <= 0)
            {
                playerRenderer1.enabled = true;
                playerRenderer2.enabled = true;
                playerRenderer3.enabled = true;
                //playerRenderer4.enabled = true;
            }

        }

        if (isFadeToBlack)
        {
            blackScreen.color = new Color(blackScreen.color.r, blackScreen.color.g, blackScreen.color.b, Mathf.MoveTowards(blackScreen.color.a, 1f, fadeSpeed * Time.deltaTime));
            if(blackScreen.color.a == 1f)
            {
                isFadeToBlack = false;
            }
        }
        if (isFadeFromBlack)
        {
            blackScreen.color = new Color(blackScreen.color.r, blackScreen.color.g, blackScreen.color.b, Mathf.MoveTowards(blackScreen.color.a, 0f, fadeSpeed * Time.deltaTime));
            if (blackScreen.color.a == 0f)
            {
                isFadeFromBlack = false;
            }
        }

    }

    public void Respawn()
    {
        /*
        GameObject player = GameObject.FindWithTag("AlienAndy");

        CharacterController charController = player.GetComponent<CharacterController>();

        charController.enabled = false;

        charController.transform.position = respawnPoint;

        charController.enabled = true;

        currentHealth = maxHealth;
        */
        if (!isRespawning)
        {
            StartCoroutine("RespawnCo");
        }
    }

    public IEnumerator RespawnCo()
    {
        isRespawning = true;
        alienAndy.gameObject.SetActive(false);
        yield return new WaitForSeconds(respawnLength);

        isFadeToBlack = true;

        yield return new WaitForSeconds(waitForFade);


        isFadeToBlack = false;
        isFadeFromBlack = true;

        isRespawning = false;

        alienAndy.gameObject.SetActive(true);
        GameObject player = alienAndy.gameObject;//GameObject.FindWithTag("AlienAndy");
        CharacterController charController = player.GetComponent<CharacterController>();

        charController.enabled = false;

        charController.transform.position = respawnPoint;

        charController.enabled = true;

        currentHealth = maxHealth;
        invincibilityCounter = invincibilityLength;

        playerRenderer1.enabled = false;
        playerRenderer2.enabled = false;
        playerRenderer3.enabled = false;
        //playerRenderer4.enabled = false;

        flashCounter = flashLength;

        healthText.text = "Health: " + currentHealth;
    }

    public void HurtPlayer(int damage, Vector3 direction)
    {
        if (invincibilityCounter <= 0)
        {
            currentHealth -= damage;

            if (currentHealth <= 0)
            {
                Respawn();
            }
            else
            {
                alienAndy.KnockBack(direction);

                invincibilityCounter = invincibilityLength;

                //playerRenderer1.enabled = false;
                //playerRenderer2.enabled = false;
                //playerRenderer3.enabled = false;
                //playerRenderer4.enabled = false;

                flashCounter = flashLength;
            }
        }

        healthText.text = "Health: " + currentHealth;
    }

    public void HealPlayer(int healAmount)
    {
        currentHealth += healAmount;

        if(currentHealth > maxHealth)
        {
            currentHealth = maxHealth;
        }

        healthText.text = "Health: " + currentHealth;
    }
}
