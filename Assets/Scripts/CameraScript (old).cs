﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour
{
    // Start is called before the first frame update
    private float pi = Mathf.PI;
    
    private float rot_spd = .1f;
    private float movespd = 3f;
    
    
    private float hr = 0;
    private float vr = 0;
    
    private float maxvr = Mathf.PI/2.2f;
    private float minvr = -Mathf.PI/2.2f;
    
    
    public Terrain t;
    
    void Start()
    {
        Cursor.visible = false;//Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if(Input.GetKey(KeyCode.W))
        {
            transform.position += new Vector3(movespd * Mathf.Cos(hr) , 0 , movespd * Mathf.Sin(hr));
        }
        if(Input.GetKey(KeyCode.A))
        {
            transform.position += new Vector3(movespd * Mathf.Cos(hr+pi/2f) , 0 , movespd * Mathf.Sin(hr+pi/2f));
        }
        if(Input.GetKey(KeyCode.D))
        {
            transform.position += new Vector3(movespd * Mathf.Cos(hr-pi/2f) , 0 , movespd * Mathf.Sin(hr-pi/2f));
        }
        if(Input.GetKey(KeyCode.S))
        {
            transform.position += new Vector3(movespd * Mathf.Cos(hr+pi) , 0 , movespd * Mathf.Sin(hr+pi));
        }
        if(Input.GetKey(KeyCode.Space))
        {
            transform.position += Vector3.up * movespd;
        }
        if(Input.GetKey(KeyCode.LeftShift))
        {
            transform.position += Vector3.down * movespd;
        }
        
        int width = t.terrainData.heightmapWidth;
        int height = t.terrainData.heightmapHeight;
        
        float[,] heights = t.terrainData.GetHeights(0,0,width,height);
        
        vr -= rot_spd*Input.GetAxis("Mouse Y");
        hr -= rot_spd*Input.GetAxis("Mouse X");
        
        
        
        
        
        
        
        
        //print("width"+width);
        //print("height"+height);
        setHRVR();
    }
    float degToRad(float deg)
    {
        return deg/360*2*Mathf.PI;
    }
    float radToDeg(float rad)
    {
        return rad/(Mathf.PI*2)*360;
    }
    void setHRVR()
    {
        if(vr > maxvr)
        {
            vr = maxvr;
        }
        if(vr < minvr)
        {
            vr = minvr;
        }
        transform.eulerAngles = new Vector3(radToDeg(vr),-radToDeg(hr)+90,0f);
    }
    
}
