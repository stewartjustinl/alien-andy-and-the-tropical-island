using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class PlayerMovement : MonoBehaviour
{
    public float speed;
    public float gravityScale;
    public float jumpForce = 7f;

    public float knockBackForce;
    public float knockBackTime;
    private float knockBackCounter;
    
    
    
    public CapsuleCollider col;
    public LayerMask groundLayers;
    public CharacterController controller;
    private Vector3 moveDirection;
    //float verticalMomentum = 0;

    public Animator m_Animator;
    public Transform pivot;
    public float rotateSpeed;

    public GameObject playerModel;

    AudioSource m_AudioSource;
    AudioSource AAWalking;

    Vector3 movement;
    Quaternion rotation = Quaternion.identity;
    private Transform cameraTransform;
    
    
    int swordCooldown = -1;//just a timer used in update
    int swordDelay = -1;//just a timer used in update
    int swordCooldownTime = 30;//how long in between sword strikes?
    int swordDelayTime = 15;//how long between clicking and the attack actually carrying through?


    public Transform attackPoint;
    private float attackRange = 4f;
    public LayerMask enemyLayers;

    private void Awake()
    {
        
    }
    void Start()
    {
        Physics.gravity = new Vector3(0,-15f,0);
        m_Animator = GetComponent<Animator>();
        
        m_AudioSource = GetComponent<AudioSource>();
        col = GetComponent<CapsuleCollider>();
        controller = GetComponent<CharacterController>();
        //if(GlobalControl.Instance.scene == GlobalControl.Environment.Dungeon1)
        if(SceneManager.GetActiveScene().name == "Dungeon1")
        {
            transform.position = new Vector3(64.63f, -25.99f, -16.98f);        
        }
        else if(SceneManager.GetActiveScene().name == "Dungeon2")
        {
            transform.position = new Vector3(68.37f, -28.85f, -21.56f);
        }
        else if(GlobalControl.Instance.scene == GlobalControl.Environment.Overworld)
        //else if(SceneManager.GetActiveScene().name == "AlienAndy")
        {
            transform.position = new Vector3(433.7f, 3.7f, 308.1f);
        }
        //if (GlobalControl.Instance.scene = GlobalControl.Environment.Dungeon1) { }
        //cameraTransform = Camera.main.transform;

    }
    
    
    
    
    
    
    public float GetDamage()
    {
        Item firstItem = GetComponent<Inventory>().inventoryUI.UI_Items[0].item;
        if(firstItem == null)
        {
            return 0;
        }
        if(firstItem.stats.ContainsKey("Attack"))
        {
            return firstItem.stats["Attack"];
        }
        return 0;
    }
    
    
    
    
    
    public void Damage(float damage)
    {
        //print("ow");
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.K))
        {
            FindObjectOfType<HealthManager>().HurtPlayer(20,Vector3.down);
        }
        setPosition();
       
        swordCooldown -= 1;
        swordDelay -= 1;
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        if (Input.GetButtonDown( "Horizontal" ) || Input.GetButtonDown( "Vertical"))
        {
            m_AudioSource.Play();
        }
        else if (!Input.GetButton("Horizontal") && !Input.GetButton("Vertical") && m_AudioSource.isPlaying)
        {
            m_AudioSource.Stop();
        }

        //moveDirection = new Vector3(moveHorizontal * speed, moveDirection.y, moveVertical * speed);
        if (knockBackCounter <= 0)
        {
            float tempSpeed;
            if (Input.GetKey(KeyCode.LeftShift))
            {
                //Debug.Log("Should be sprinting");
                tempSpeed = 2 * speed;
                m_Animator.SetBool("Sprinting", true);
                

            }
            else
            {
                tempSpeed = speed;
                m_Animator.SetBool("Sprinting", false);
            }
            float yStore = moveDirection.y;
            moveDirection = (transform.forward * moveVertical) + (transform.right * moveHorizontal);
            moveDirection = moveDirection.normalized * tempSpeed;
            moveDirection.y = yStore;
            //print(playerModel.GetComponent<Transform>().rotation.eulerAngles.y);
            if (Input.GetMouseButtonDown(0) && swordCooldown <= 0)
            {
                swordCooldown = swordCooldownTime;
                swordDelay = swordDelayTime;
                m_Animator.SetBool("Swinging", true);
                
                
                GameObject cam = GetTag("MainCamera")[0];
                
                
                
                
                playerModel.GetComponent<Transform>().eulerAngles = new Vector3(
                playerModel.GetComponent<Transform>().eulerAngles.x,
                cam.GetComponent<CameraController>().desiredYAngle + 180,
                playerModel.GetComponent<Transform>().eulerAngles.z);
                    
                    
            }
            else
            {
                m_Animator.SetBool("Swinging", false);
            }


            if (controller.isGrounded)
            {
                //verticalMomentum = 0f;
                if (controller.isGrounded && Input.GetKey(KeyCode.Space))
                {
                    moveDirection.y = jumpForce;
                }
            }
            if (swordDelay == 0)
            {
                attack();
            }
        }else
        {
            knockBackCounter -= Time.deltaTime;
        }






        moveDirection.y = moveDirection.y + (Physics.gravity.y * gravityScale * Time.deltaTime);
        controller.Move(moveDirection * Time.deltaTime);

        //This is the bit that changes the players rotation based off of the camera
        if (Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0)
        {
            float yRotation = pivot.eulerAngles.y;
            if(GameObject.FindGameObjectsWithTag("MainCamera")[0].GetComponent<CameraController>().desiredXAngle >= 90)//fix the reverse direction problem (peter)
            {
                yRotation += 180;
            }
            transform.rotation = Quaternion.Euler(0f, yRotation, 0f);
            Quaternion newRotation = Quaternion.LookRotation(new Vector3(moveDirection.x, 0f, moveDirection.z));
            playerModel.transform.rotation = Quaternion.Slerp(playerModel.transform.rotation, newRotation, rotateSpeed * Time.deltaTime);
        }


        m_Animator.SetFloat("Speed", Mathf.Abs(Input.GetAxis("Horizontal")) + Mathf.Abs(Input.GetAxis("Vertical")));
    }
    float HDistance(Vector3 a, Vector3 b){ return Sqrt(Sq(a.x-b.x) + Sq(a.z-b.z)); }
    float Sq(float f) { return f*f; }
    float Sqrt(float f) { return Mathf.Sqrt(f); }
    float HAngle(Vector3 a, Vector3 b) { return _GetAngle(a.z,a.x,b.z,b.x); } float _GetAngle(float z1, float x1, float z2, float x2) { if(x1==x2) { if(z1>=z2) { return Mathf.PI/2; } else { return 3*Mathf.PI/2; } } if(x1 > x2) return Mathf.Atan((z2-z1)/(x2-x1)) + Mathf.PI; else return Mathf.Atan((z2-z1)/(x2-x1)); }
    float AngleDistanceRad(float a1, float a2){float ret = abs(a1-a2);while(ret >= Mathf.PI*2)ret = ret - Mathf.PI * 2;while(ret < 0)ret = ret + Mathf.PI * 2;if(ret > Mathf.PI)ret = 2*Mathf.PI - ret;return ret;}
    float AngleDistanceDeg(float a1, float a2){float ret = abs(a1-a2);while(ret >= 360)ret = ret - 360;while(ret < 0)ret = ret + 360;if(ret > 180)ret = 360 - ret;return ret;}
    float abs(float f){if(f >= 0) return f; else return -f;}
    Vector3 HMove(float angle, float distance) { return new Vector3(distance*Mathf.Cos(angle),0,distance*Mathf.Sin(angle)); }
    T g<T>() { return GetComponent<T>(); }
    Transform tf() {return GetComponent<Transform>();}
    Transform tf(GameObject other) {return other.GetComponent<Transform>();}
    float RadToDeg(float rad) { return rad / (2 * Mathf.PI) * 360; }
    float DegToRad(float deg) { return deg / 360 * (2 * Mathf.PI); }
    float UnityToUnitCircle(float deg) { return -(DegToRad(deg) - Mathf.PI / 2); }
    float UnitCircleToUnity(float rad) { return RadToDeg(-rad + Mathf.PI / 2); }
    void attack()
    {
        
        foreach(GameObject enemy in GameObject.FindGameObjectsWithTag("Enemy"))
        {
            if(Vector3.Distance(enemy.transform.position, gameObject.transform.position) < attackRange)
            {
                //check if enemy is in front player
                if(45 > AngleDistanceDeg(UnitCircleToUnity(HAngle(GetComponent<Transform>().position, enemy.transform.position)), playerModel.GetComponent<Transform>().eulerAngles.y))
                {
                    if(enemy.GetComponent<EnemyScript>() != null) enemy.GetComponent<EnemyScript>().takeDamage(GetDamage());
                    if(enemy.GetComponent<NewEnemyScript>() != null)enemy.GetComponent<NewEnemyScript>().takeDamage(GetDamage());
                }
                
            }
        }
        //sorry ben, for some reason with this code the enemies weren't being hit even when 
        //right up against the player, in certain situations
        //So, I'm debugging this now
        /*
        Collider[] hitEnemies = Physics.OverlapSphere(attackPoint.position, attackRange, enemyLayers);

        foreach(Collider enemy in hitEnemies)
        {
            
            if(enemy.GetComponent<EnemyScript>() != null) enemy.GetComponent<EnemyScript>().takeDamage(10);
            if(enemy.GetComponent<NewEnemyScript>() != null)enemy.GetComponent<NewEnemyScript>().takeDamage(10);
        }
        */
    }

    void OnDrawGizmosSelected()
    {
        if(attackPoint == null)
        {
            return;
        }
        Gizmos.DrawWireSphere(attackPoint.position, attackRange);
    }

    private bool IsGrounded()
    {
        return Physics.CheckCapsule(col.bounds.center, new Vector3(col.bounds.center.x, col.bounds.min.y, col.bounds.center.z), col.radius*.9f, groundLayers);
    }

    void OnAnimatorMove()
    {
        /* //print("Got to animator move!");
        rb.MovePosition(rb.position + movement * speed );
        rb.MoveRotation(rotation);*/
    }

    public void KnockBack(Vector3 direction)
    {
        knockBackCounter = knockBackTime;

        moveDirection = direction * knockBackForce;
        moveDirection.y = knockBackForce;
    }

    public void getPosition()
    {
        transform.position = GlobalControl.Instance.position;
    }

    public void setPosition()
    {
        GlobalControl.Instance.position = transform.position;
        //Debug.Log("Running this");
    }

    GameObject[] GetTag(string tag){return GameObject.FindGameObjectsWithTag(tag);}
}
