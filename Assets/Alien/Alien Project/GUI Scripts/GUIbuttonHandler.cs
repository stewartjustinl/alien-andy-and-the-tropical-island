using UnityEngine;
using System.Collections;

public class GUIbuttonHandler : MonoBehaviour {

	
	private GameObject alien;	
	private Animator anim;  
	//private string animPlaying = "";

	void Start () 
	{
		alien = GameObject.Find("Alien");
		anim = alien.GetComponent<Animator>(); 
	}
	
    void OnGUI() 
	{
		if (anim.GetBool("longIdle")){anim.SetBool("longIdle",false);}
		if (anim.GetBool("crouch")){anim.SetBool("crouch",false);}
		if (anim.GetBool("jump")){anim.SetBool("jump",false);}
		if (anim.GetInteger("rotate") !=0 ){anim.SetInteger("rotate",0);}
		if (anim.GetBool("taunt")){anim.SetBool("taunt",false);}
		if (anim.GetBool("dodgeRight")){anim.SetBool("dodgeRight",false);}
		if (anim.GetInteger("attackID") !=0 ){anim.SetInteger("attackID",0);}
		if (anim.GetBool("hit")){anim.SetBool("hit",false);}
		if (anim.GetBool("death")){anim.SetBool("death",false);}
		if (anim.GetInteger("defend") !=0 ){anim.SetInteger("defend",0);}

		//if (anim.GetBool("attackReady")){anim.SetBool("attackReady",false);}
		float buttonWidth = 120;
		float buttonHeight = 50;
		float leftMargin = Screen.width*.1f;
		float rightMargin = Screen.width - (leftMargin+buttonWidth);
		float bottomMargin = Screen.height *.9f;
		if (GUI.Button(new Rect(leftMargin, 30, buttonWidth, buttonHeight), "Idle")){OnMouseDown("Idle");}

		if (GUI.Button(new Rect(leftMargin, 90, buttonWidth, buttonHeight), "Long Idle")){OnMouseDown("LongIdle");}

		if (GUI.Button(new Rect(leftMargin, 150, buttonWidth, buttonHeight), "Walk")){OnMouseDown("Walk");}

		if (GUI.Button(new Rect(leftMargin, 210, buttonWidth, buttonHeight), "Run")){OnMouseDown("Run");}

		if (GUI.Button(new Rect(leftMargin, 270, buttonWidth, buttonHeight), "Walk Backwards")){OnMouseDown("walkReverse");}

		if (GUI.Button(new Rect(leftMargin, 330, buttonWidth, buttonHeight), "Crouch")){OnMouseDown("crouch");}   

		if (GUI.Button(new Rect(leftMargin, 390, buttonWidth, buttonHeight), "Crouch Walk")){OnMouseDown("crouchWalk");}   

		if (GUI.Button(new Rect(leftMargin, 450, buttonWidth, buttonHeight), "Jump")){OnMouseDown("jump");}  

		if (GUI.Button(new Rect(leftMargin, 510, buttonWidth, buttonHeight), "Rotate Left")){OnMouseDown("rotateLeft");}   

		if (GUI.Button(new Rect(leftMargin, 570, buttonWidth, buttonHeight), "Roate Right")){OnMouseDown("rotateRight");}   

		//defense
		if (GUI.Button(new Rect(leftMargin + buttonWidth, bottomMargin, buttonWidth, buttonHeight), "Block High")){OnMouseDown("blockHigh");}   

		if (GUI.Button(new Rect(leftMargin + (buttonWidth*2), bottomMargin, buttonWidth, buttonHeight), "Dodge Left")){OnMouseDown("dodgeLeft");}   

		if (GUI.Button(new Rect(leftMargin + (buttonWidth*3), bottomMargin, buttonWidth, buttonHeight), "Dodge Right")){OnMouseDown("dodgeRight");}   

		//damage
		if (GUI.Button(new Rect(leftMargin + (buttonWidth*5), bottomMargin, buttonWidth, buttonHeight), "Hit")){OnMouseDown("hit");}   
		
		if (GUI.Button(new Rect(leftMargin + (buttonWidth*6), bottomMargin, buttonWidth, buttonHeight), "Death")){OnMouseDown("death");}    

		//Attack

		if (GUI.Button(new Rect(rightMargin, 30, buttonWidth, 50), "Taunt")){OnMouseDown("taunt");}
		if (GUI.Button(new Rect(rightMargin, 150, buttonWidth, 50), "Attack Ready")){OnMouseDown("attackReady");}
		if (GUI.Button(new Rect(rightMargin, 270, buttonWidth, 50), "Attack #1")){OnMouseDown("attack01");}
		if (GUI.Button(new Rect(rightMargin, 330, buttonWidth, 50), "Attack #2")){OnMouseDown("attack02");}   
		if (GUI.Button(new Rect(rightMargin, 390, buttonWidth, 50), "Attack #3")){OnMouseDown("attack03");}   
		if (GUI.Button(new Rect(rightMargin, 450, buttonWidth, 50), "Throw Spear")){OnMouseDown("throwSpear");}   
		

	}

	void Update()
	{

	}

	void OnMouseDown(string animType)
	{

		//anim.SetInteger("walkSpeed", 0);

		//animPlaying = animType;


		if (animType == "Idle") 		
		{
			anim.SetBool("attackReady", false);
			anim.SetInteger("walkSpeed", 0);
			if (anim.GetBool("crouchWalk")){anim.SetBool("crouchWalk",false);}
		}

		if (animType == "LongIdle") 	
		{ 
			anim.SetInteger("walkSpeed", 0); 
			anim.SetBool("longIdle", true);
		}	

		if (animType == "walkReverse") 			
		{
			if (anim.GetBool("crouchWalk")){anim.SetBool("crouchWalk",false);}
			anim.SetInteger("walkSpeed", -1);
		}

		if (animType == "Walk") 			
		{
			if (anim.GetBool("crouchWalk")){anim.SetBool("crouchWalk",false);}
			anim.SetInteger("walkSpeed", 1);
		}

		if (animType == "Run") 			
		{
			if (anim.GetBool("crouchWalk")){anim.SetBool("crouchWalk",false);}
			anim.SetInteger("walkSpeed", 2);
		}

		if (animType == "crouch") 			
		{
			anim.SetInteger("walkSpeed", 0); 
			anim.SetBool("crouch", true);
			if (anim.GetBool("crouchWalk")){anim.SetBool("crouchWalk",false);}

		}

		if (animType == "crouchWalk") 			
		{
			anim.SetInteger("walkSpeed", 0); 

			anim.SetBool("crouch", true);
			anim.SetBool("crouchWalk", true);

		}

		if (animType == "jump") 			
		{
			anim.SetBool("jump",true); 
			if (anim.GetBool("crouchWalk")){anim.SetBool("crouchWalk",false);}
			
		}

		if (animType == "rotateLeft") 			
		{
			anim.SetInteger("walkSpeed", 0); 
			if (anim.GetBool("crouchWalk")){anim.SetBool("crouchWalk",false);}
			anim.SetInteger("rotate",-1); 
		}

		if (animType == "rotateRight") 			
		{
			anim.SetInteger("walkSpeed", 0); 
			if (anim.GetBool("crouchWalk")){anim.SetBool("crouchWalk",false);}
			anim.SetInteger("rotate",1); 
		}

		if (animType == "taunt") 	
		{ 
			anim.SetBool("taunt", true);
		}	

		if (animType == "dodgeRight") 	
		{ 
			anim.SetBool("dodgeRight", true);
		}	

		if (animType == "attackReady") 	
		{ 
			anim.SetInteger("attackID", 0);
			anim.SetBool("attackReady", true);
		}	

		if (animType == "attack01") 	
		{ 
			anim.SetBool("attackReady", true);
			anim.SetInteger("attackID", 1);
		}	

		if (animType == "attack02") 	
		{ 
			anim.SetBool("attackReady", true);
			anim.SetInteger("attackID", 2);
		}	

		if (animType == "attack03") 	
		{ 
			anim.SetBool("attackReady", true);
			anim.SetInteger("attackID", 3);
		}

		if (animType == "throwSpear") 	
		{ 
			anim.SetBool("attackReady", true);
			anim.SetInteger("attackID", 4);
		}	

		if (animType == "hit") 	
		{ 
			anim.SetBool("hit", true);
		}	

		if (animType == "death") 	
		{ 
			anim.SetBool("death", true);
		}	

		if (animType == "blockHigh") 	
		{ 
			anim.SetBool("attackReady", true);
			anim.SetInteger("defend", 1);
		}	

		if (animType == "dodgeLeft") 	
		{ 
			anim.SetBool("attackReady", true);
			anim.SetInteger("defend", 2);
		}

		if (animType == "dodgeRight") 	
		{ 
			anim.SetBool("attackReady", true);
			anim.SetInteger("defend", 3);
		}

	}
}