using UnityEngine;
using System.Collections;

public class ButtonHandler : MonoBehaviour {
	

	private GameObject demon;	
	private Animator anim;   
	// Use this for initialization
	void Start () 
	{
		demon = GameObject.Find("Demon");
		anim = demon.GetComponent<Animator>(); 
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}
	
	void OnMouseDown()
	{
		
		anim.SetBool("Walk", false);
		anim.SetBool("Attack", false);
		anim.SetBool("Attack2", false);
		anim.SetBool("Hit", false);
		anim.SetBool("Death", false);

		
		
		if (this.name == "IdleButton") 		{anim.SetBool("Idle", true);}
		if (this.name == "WalkButton") 		{anim.SetBool("Walk", true);}
		if (this.name == "Attack1Button") 	{anim.SetBool("Attack", true);}
		if (this.name == "Attack2Button") 	{anim.SetBool("Attack2", true);}
		if (this.name == "HitButton") 		{anim.SetBool("Hit", true);}
		if (this.name == "DeathButton") 	{anim.SetBool("Death", true);}
		
		
	}
	
}
